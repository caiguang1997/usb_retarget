# USB Retarget



## 1 介绍

USB设备重定向，包括键盘、鼠标、手柄等。



## 2 工程目录及说明

```
.
├── hid_device          # HID设备重定向（包括键盘、鼠标等）
│   ├── hidapi
│   ├── README.md
│   └── user
├── joystick_device     # 手柄重定向
│   ├── jstest-gtk
│   ├── README.md
│   └── user
└── README.md           # 本文件
```



## 3 使用说明

下载代码

```sh
git clone --recursive https://gitee.com/caiguang1997/usb_retarget.git
```



## 4 日志

```
✔️ 2020.11.12 测试发现“手柄”并不属于HID类，因此新增对“手柄”的支持
✔️ 2020.11.11 测试通过自己的鼠标（HID类），下一步将要进行无线迷你键盘测试
```



## 5 参与贡献

```
蔡广
	QQ:     2240726829
	wechat: 2240726829
	email:  2240726829@qq.com
```
