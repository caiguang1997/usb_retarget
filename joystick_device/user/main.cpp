#include <iostream>
#include <sstream>
#include <unistd.h>
#include <fcntl.h>
#include <linux/joystick.h>
#include <signal.h>

using namespace std;

int device;     //设备句柄

void handler(int signal)
{
    close(device);
}

void update(void)
{
    static struct js_event event;

    ssize_t len = read(device, &event, sizeof(event));

    if ( sizeof(js_event) == len )
    {
        if (event.type & JS_EVENT_AXIS)
        {
            cout << "Axis: " << (int)event.number << " -> " << (int)event.value << endl;            //轴值更改
        }
        else if (event.type & JS_EVENT_BUTTON)
        {
            cout << "Button: " << (int)event.number << " -> " << (int)event.value << endl;          //按键值更改
        }
    }
    else
    {
        throw runtime_error("- - - - - Joystick Update: Read Error! - - - - -\n");                  //报错
    }
}

int main(int argc, char* argv[])
{
    signal(SIGINT, handler);

    char name[128];         //手柄名
    uint8_t num_axis   = 0; //轴数量
    uint8_t num_button = 0; //按键数

    //找设备
    int i;
    for(i = 0; i < 32; ++i)
    {
        ostringstream str;
        str << "/dev/input/js" << i;
        device = open(str.str().c_str(), O_RDONLY);
        if(device >= 0)
        {
            ioctl(device, JSIOCGNAME(sizeof(name)), name);  //查找手柄名
            ioctl(device, JSIOCGAXES, &num_axis);            //查找轴数量
            ioctl(device, JSIOCGBUTTONS, &num_button);          //查找按键数
            cout<<"- - - - - "<<str.str().c_str()<<" found! - - - - -"<<endl;
            cout<<"Name:\t\t"<<name<<endl;
            cout<<"Axis num:\t"<<(int)num_axis<<endl;
            cout<<"Button num:\t"<<(int)num_button<<endl;
            cout<<"- - - - - - - - - - - - - - - - - - - - -"<<endl;
            break;  //使用找到的第一个设备
        }
    }

    if( i==32 && device < 0 )
    {
        cout<<"- - - - - Not Found! - - - - -"<<endl;
        return 1;
    }

    //更新数据
    while (1)
    {
        update();
    }

    return 0;
}
