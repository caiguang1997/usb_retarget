# USB Joystick Retarget



## 1 介绍

1. **手柄（Joystick或gamepad）**一般不需要安装驱动，因为**系统自带驱动**；
2. 目前手柄厂家较多，品牌如XBox（微软）、Logitech（罗技）、Razer（雷蛇）等，本项目测试的手柄为国产**北通阿修罗无线手柄BTP-2185**，该手柄使用驱动为**XPad驱动**即XBox一类；
3. 目前，本项目**只测试过Xbox类手柄**，对Logitech、Razer等未测试，但由于Logitech、Razer都是有开源驱动程序的，所以，自定义难度本身并不大，因此，不再赘述；



## 2 工程目录及说明

```
.
├── jstest-gtk              # 开源驱动程序（带GUI，测试很方便），本项目主要参考该代码
│   ├── CMakeLists.txt
│   ├── COPYING
│   ├── data
│   ├── io.gitlab.jstest_gtk.jstest_gtk_nightly.json
│   ├── jstest-gtk.1
│   ├── jstest-gtk.appdata.xml.in
│   ├── jstest-gtk.desktop
│   ├── jstest-gtk.sh.in
│   ├── jstest-gtk.svg
│   ├── NEWS
│   ├── README.md
│   ├── shared-modules
│   ├── src
│   └── TODO
├── README.md               # 本文件
└── user                    # 本项目
    ├── CMakeLists.txt
    └── main.cpp
```



## 3 使用说明

**Ubuntu 16.04**上使用流程如下：

1. 下载代码

```sh
git clone --recursive https://gitee.com/caiguang1997/usb_retarget.git
```

2. 查看驱动类型

```sh
sudo cat /sys/kernel/debug/usb/devices
```

输出如

```sh
T:  Bus=01 Lev=01 Prnt=01 Port=02 Cnt=02 Dev#= 45 Spd=12   MxCh= 0
D:  Ver= 2.00 Cls=ff(vend.) Sub=ff Prot=ff MxPS= 8 #Cfgs=  1
P:  Vendor=045e ProdID=028e Rev= 6.25
S:  Product=Controller
S:  SerialNumber=00000000
C:* #Ifs= 4 Cfg#= 1 Atr=a0 MxPwr=500mA
I:* If#= 0 Alt= 0 #EPs= 2 Cls=ff(vend.) Sub=5d Prot=01 Driver=xpad
E:  Ad=81(I) Atr=03(Int.) MxPS=  32 Ivl=4ms
E:  Ad=02(O) Atr=03(Int.) MxPS=  32 Ivl=8ms
I:* If#= 1 Alt= 0 #EPs= 2 Cls=ff(vend.) Sub=5d Prot=03 Driver=(none)
E:  Ad=83(I) Atr=03(Int.) MxPS=  32 Ivl=2ms
E:  Ad=04(O) Atr=03(Int.) MxPS=  32 Ivl=4ms
I:* If#= 2 Alt= 0 #EPs= 1 Cls=ff(vend.) Sub=5d Prot=02 Driver=(none)
E:  Ad=86(I) Atr=03(Int.) MxPS=  32 Ivl=16ms
I:* If#= 3 Alt= 0 #EPs= 0 Cls=ff(vend.) Sub=fd Prot=13 Driver=(none)
```

其中`Driver=xpad`表示驱动类型为`xpad`，即XBox一类的驱动。

3. 测试

```sh
cd ./joystick_device/user
mkdir cmake-build-debug
cd cmake-build-debug
cmake .. && make
./joystick
```

4. 输出

```sh
- - - - - /dev/input/js0 found! - - - - -
Name:		Microsoft X-Box 360 pad
Axis num:	8
Button num:	11
- - - - - - - - - - - - - - - - - - - - -
Button: 0 -> 0
Button: 1 -> 0
Button: 2 -> 0
Button: 3 -> 0
Button: 4 -> 0
Button: 5 -> 0
Button: 6 -> 0
Button: 7 -> 0
Button: 8 -> 0
Button: 9 -> 0
Button: 10 -> 0
Axis: 0 -> 0
Axis: 1 -> 0
Axis: 2 -> -32767
Axis: 3 -> 0
Axis: 4 -> 0
Axis: 5 -> -32767
Axis: 6 -> 0
Axis: 7 -> 0
```

5. 输出解释

   * `/dev/input/js0`表示joystick在系统的路径，`Name`表示手柄标识名称，`Axis num`表示手柄的轴数，`Button num`表示手柄的按键数量
   * `Button: 10 -> 0`表示按键10的当前值为0，`Axis: 2 -> -32767`表示轴2的当前值为-32767



## 4 日志

```
✔️ 2020.11.12 测试发现“手柄”并不属于HID类，因此新增对“手柄”的支持
```



## 5 参与贡献

```
蔡广
	QQ:     2240726829
	wechat: 2240726829
	email:  2240726829@qq.com
```
