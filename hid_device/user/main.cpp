#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <hidapi/hidapi.h>
#include <thread>
#include <chrono>

using namespace std;

void readData(const char* name, hid_device* handle)
{
    unsigned char buf[128]; //数据缓存区
    unsigned int cnt = 1;   //第cnt次获取数据

    while (1)
    {
        if (handle)
        {
            int len = hid_read(handle, buf, sizeof(buf));
            if (len>0)
            {
                printf("%s, cnt: %d\t\tData read: ", name, cnt++);
                for (int i = 0; i < len - 1; i++) printf("%02x,", buf[i]);
                printf("%02x\n", buf[len - 1]);
            }
        }
    }
}

class hid_handle
{
public:
    const char* m_name;         //设备名
    hid_device* m_handle;       //设备句柄

    hid_handle(const char* name, const char *path)
    {
        m_name = name;
        m_handle = hid_open_path(path);

        if(m_handle) printf("\n- - - - - - open %s device successfully! - - - - - -\n\n", m_name);
        else printf("- - - - - - open %s device failed! - - - - - -\n", m_name);
    }

    ~hid_handle()
    {
        hid_close(m_handle);
    }

    static char* search_for_hid_path(struct hid_device_info* all_hid, unsigned short vendor_id,
                                     unsigned short product_id, int interface_number)
    {
        struct hid_device_info* cur_hid = all_hid;
        while(cur_hid)
        {
            if(cur_hid->vendor_id==vendor_id&&
               cur_hid->product_id==product_id&&
               cur_hid->interface_number==interface_number)
            {
                return cur_hid->path;
            }
            cur_hid = cur_hid->next;
        }
        printf("Can't Find Path, Error!\n");
        exit(1);
    }
};

struct hid_device_info * all_dev;  //设备集合（全部设备）
hid_device* handle;             //设备句柄（单个设备）

//意外中断，释放鼠标
void signal_handler(int signal)
{
    hid_free_enumeration(all_dev);
    hid_close(handle);
    hid_exit();
}

int main(int argc, char* argv[])
{
//    signal(SIGINT, signal_handler);
//    signal(SIGSTOP, signal_handler);
//    signal(SIGQUIT, signal_handler);

    if (hid_init()) return -1;

    //获取并打印可用设备
    all_dev = hid_enumerate(0x0, 0x0);
    struct hid_device_info * cur_dev = all_dev;
    while (cur_dev)
    {
        printf("Device Found\n"
               "  path: %s\n"
               "  Manufacturer: %ls\n"
               "  Product: %ls\n"
               "  Vendor ID: %d\n"
               "  Product ID: %d\n"
               "  Interface Number: %d\n\n",
               cur_dev->path,
               cur_dev->manufacturer_string, cur_dev->product_string,
               cur_dev->vendor_id, cur_dev->product_id, cur_dev->interface_number);
        cur_dev = cur_dev->next;
    }

    char* key_path = hid_handle::search_for_hid_path(all_dev, 1299, 792, 0);
    char* mouse_path = hid_handle::search_for_hid_path(all_dev, 1299, 792, 1);

    hid_handle key("key", key_path);
    hid_handle mouse("mouse", mouse_path);

    thread key_thread(readData, key.m_name, key.m_handle);
    thread mouse_thread(readData, mouse.m_name, mouse.m_handle);

    key_thread.join();
    mouse_thread.join();

    while (1);

    return 0;
}