# USB HID Retarget



## 1 介绍

1. **HID**（Human Interface Device，人际接口设备）是**USB从机设备**的一种类型，键盘、鼠标、CMSIS DAP等一般都属于这一类，特点是**硬件设备跨平台**且**无需驱动**，使用github开源驱动程序**hidapi**（https://github.com/signal11/hidapi）;
2. 由于**硬件设备跨平台**且**无需驱动**的特性，因此很容易将代码稍加修改即可**实现任意设备的抓包（数据截获）、数据重定向为自己设置的功能**;
3. 目前该程序**仅在Ubuntu 16.04上测试通过**，其他平台应该类同（hidapi是跨平台的）。



## 2 工程目录及说明

```
.
├── hidapi								//开源HID设备驱动
│   ├── android
│   ├── AUTHORS.txt
│   ├── bootstrap
│   ├── configure.ac
│   ├── doxygen
│   ├── HACKING.txt
│   ├── hidapi
│   ├── hidtest							//官方测试文件，可以参考下
│   ├── libusb
│   ├── LICENSE-bsd.txt
│   ├── LICENSE-gpl3.txt
│   ├── LICENSE-orig.txt
│   ├── LICENSE.txt
│   ├── linux							//linux平台相关
│   ├── mac								//mac平台相关
│   ├── windows							//windows平台相关
│   ├── m4
│   ├── Makefile.am
│   ├── pc
│   ├── README.txt
│   ├── testgui
│   └── udev
├── README.md							//本文件
└── user								//用户代码
    ├── CMakeLists.txt
    └── main.cpp


```



## 3 使用说明

**Ubuntu 16.04**上使用流程如下：

1. 下载代码

```sh
git clone --recursive https://gitee.com/caiguang1997/usb_retarget.git
```

2. 编译安装hidapi

```sh
cd ./hid_device/hidapi
./bootstrap
./configure
make
sudo make install
```

3. 测试

```sh
cd ./hid_device/user
mkdir cmake-build-debug
cd cmake-build-debug
cmake .. && make
./device
```

4. 输出

```sh
Device Found
  path: 0001:0028:00
  Manufacturer: MI Dongle
  Product: MI Wireless Mouse
Device Found
  path: 0001:0028:01
  Manufacturer: MI Dongle
  Product: MI Wireless Mouse
Device Found
  path: 0001:0028:02
  Manufacturer: MI Dongle
  Product: MI Wireless Mouse
Device Found
  path: 0001:0028:03
  Manufacturer: MI Dongle
  Product: MI Wireless Mouse

- - - - - - open device successfully! - - - - - -

cnt: 0		Data read: 02,00,10,00
cnt: 1		Data read: 02,00,10,00
cnt: 2		Data read: 02,ff,2f,00
cnt: 3		Data read: 02,00,10,00

```

5. 输出解释

   * `Device Found`表示找到的设备，有几个`Device Found`表示有几个**HID设备枚举**，实际上我这里只用了一个**无线鼠标**，但是它也枚举了4个HID出来，所以我们要确定哪个设备才是我们需要的；

   * `user/main.c`文件39行的`handle = hid_open_path(all_dev->next->path);`表示打开特定路径下的设备，因此，你也可以写成`handle = hid_open_path(“0001:0028:01”)`，其中`0001:0028:01`来自`path: 0001:0028:01`，通过不断测试所有设备，即可找到自己想要的那个；
   * `cnt: 0		Data read: 02,00,10,00`表示从设备上得到的数据；



## 4 日志

```
✔️ 2020.11.12 测试发现“手柄”并不属于HID类，因此新增对“手柄”的支持
✔️ 2020.11.11 测试通过自己的鼠标，下一步将要进行无线迷你键盘测试
```



## 5 参与贡献

```
蔡广
	QQ:     2240726829
	wechat: 2240726829
	email:  2240726829@qq.com
```
